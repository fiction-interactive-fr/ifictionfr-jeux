"La route des vins" by "Otto Grimwald"

The release number is 3.
The story creation year is 2008.
The story genre is "Slice of life".
The story headline is "Une aventure dans les montagnes portugaises".

The story description is "Histoire réalisée en 5/6h heures le 1 juin 2008 pour le 3ème Speedif francophone. Les thèmes à utiliser étaient Heleophryne Natalensis (un batracien), Vila Cova da Lixa (une ville portugaise), Baal (un dieu oublié) et Citroën SM (une voiture). Reprise et amélioration du code quelques jours plus tard.".

[
http://fr.wikipedia.org/wiki/Heleophryne_natalensis
http://fr.wikipedia.org/wiki/Vila_Cova_da_Lixa
http://fr.wikipedia.org/wiki/Baal
http://fr.wikipedia.org/wiki/Citro%C3%ABn_SM
]

Part 1 - Mise en place des options

Include French by Eric Forgeot.
Use French language.

Include Boussole by Benjamin Roux.

Rule for deciding whether all includes scenery: it does not.

Use full-length room descriptions.
The maximum score is 4.

After taking or examining a thing (called truc):
	now truc is described;
	continue the action.
	
Instead of exiting:
	now carnet is described;
	now chandail is described;
	continue the action.

Instead of outgoing citroen, try exiting.

Instead of downgoing when the player is inside citroen, try exiting.

Instead of upgoing when citroen is in location, try entering citroen.

Instead of going inside when citroen is in location, try entering citroen.

An animal can be tamed or untamed. An animal is usually untamed.

A thing can be coupant or emoussé. A thing is usually emoussé.

Hinting is an action applying to nothing.
Understand "astuces" or "astuce" or "hint" or "hints" as hinting.

Carry out hinting:
	say "Dans ce jeu vous devez trouver un moyen de réparer votre voiture, et éventuellement de ne pas trop perdre vos précieuses affaires.[paragraph break]";
	if the location is in BordureDeRoute and Citroen is not in Petite route descendante, say "-> Pour le moment tout va bien, mais cela ne va pas durer. Essayez de continuer la route vers le sud pour voir...";
	if the location is in BordureDeRoute and Citroen is in Petite route descendante, say "-> Essayez d[']examiner votre voiture et d[']ouvrir ses divers compartiments.";
	if the location is in Foret, say "-> Il vous faudra récupérer le pneu qui se trouve sur la pierre noire, et bricoler une nouvelle courroie pour votre voiture à partir de là.";
	if the roue is not on pierre noire, say "-> Si vous découpez une tranche de ce pneu, qu[']aurez-vous ?";
	if the courroie is in capot and Citroen is in Petite route descendante, say "-> Avez-vous bien examiné le moteur de votre voiture et pris tout ce qui pourrait vous être utile ailleurs ?";
	if Grenouille is in location, say "-> La grenouille a froid. Si vous la domestiquez, elle saura faire l[']acrobate pour vous.";
	say "[line break]Il n[']y a pas d[']autres astuces pour le moment, mais vous pourrez réessayer cette commande dans un autre contexte pour découvrir de nouvelles suggestions.". 

Reparing is an action applying to one thing.
Understand "reparer [something]" as reparing.

Carry out reparing:
	say "Vous n[']y arriverez pas ainsi.".

Reparing it with is an action applying to two thing.
Understand "reparer [something] avec [something preferably held]" as reparing it with.

Carry out reparing it with:
	say "Vous n[']y arriverez pas ainsi.".

Instead of reparing moteur:
	if the courroie2 is held by the player, try inserting courroie2 into capot;
	if the courroie2 is not held by the player, say "Il vous manque une pièce pour réparer ce moteur.".

Instead of reparing moteur with courroie2, try inserting courroie2 into capot.

Instead of reparing moteur with courroie, say "Celle-ci est complètement usée et inutilisable pour cette fonction.".

To say lire aide:
	say "[italic type]Cette histoire ne fonctionne pas selon la logique habituelle des livres et des romans : ici c[']est au lecteur de prendre son destin en main et de décider des actions à accomplir. Il suffit donc de taper les instructions avec des phrases simples, et dans la mesure du possible l[']histoire se modifiera selon les directives du lecteur. Tout n[']est pas possible et une trame narrative existe malgré tout, mais des histoires diverses sont malgré tout envisageables en fonction des choix effectués.";
	say "";
	say "Pour avoir des indications supplémentaires veuillez taper 'astuces['], et pour la solution complète la commande est 'solution'.";

Helping is an action applying to nothing.

Understand "aide" or "info" or "information" or "informations" or "sos" as helping.

Carry out helping:
	if the player is carrying carnet
		begin;
			say "Vous ouvrez votre carnet à la première page, et lisez : [line break][lire aide]";
		otherwise;
			say "Trouvez votre carnet[if player is in citroen and carnet is in citroen] (il ne devrait pas être bien loin, peut-être même à portée de main)[end if], et prenez-le avec vous ('x carnet / prendre carnet'). Ensuite, tapez de nouveau 'aide' ou 'info.'";
		end if.

Understand "credits" and "credit" as a mistake ("Merci à Yoruk et à Stormi pour leurs transcripts détaillés ainsi qu[']aux membres de http://ifiction.free.fr, en particuliers à ceux qui se lèvent le dimanche matin pour participer aux speedif :)")

Understand "licence" and "auteur" as a mistake ("Cette histoire sous licence BSD-by a été réalisée par Eric Forgeot 2008 (c).")

Understand "solution" and "walkthrough" as a mistake ("sud/examiner le carnet de commandes/prendre le carnet/sortir/prendre le chandail/ouvrir le capot/prendre la courroie/ouvrir le coffre/prendre la bouteille/ouest/prendre la grenouille/ouest/poser le pull/prendre la grenouille/attacher la courroie aux torches/mettre la grenouille dans la catapulte/prendre la roue/decouper la roue avec un angle/est/est/mettre la courroie dans le capot/sud")


Understand the commands "lier" and "accrocher" as "nouer".

A thing can be reachable. A thing is usually reachable.

Taking something not reachable is acting OutOfReach. Pushing or pulling or touching or rubbing or turning or tasting or smelling or searching or cutting or switching on or switching off or opening or closing or squeezing or burning or kissing or waving something not reachable is acting OutOfReach.

Instead of acting OutOfReach:
	say "C[']est hors de portée.". 


Casting xyzzy is an action applying to nothing.

Understand "xyzzy" as casting xyzzy.	

Carry out casting xyzzy: 
	if citroen is in location
		begin;
			move cledouze to location;
			say "[description du sort]";
			say "[one of]Pas de chance, votre belle voiture se trouvait juste en dessous, et cela a fait une petite bosse sur le capot.[or][stopping]";
		otherwise;
			move cledouze to location;
			say "[description du sort]";
		end if.
		
To say description du sort:
		say "[one of]Soudain des éclairs viennent zébrer l'atmosphère, et le tonnerre gronde un moment, et lorsque cela se calme, vous entendez un bruit sec dans les environs : quelque chose vient juste de tomber du ciel ![or]Le ciel ne gronde plus, mais la pluie se met à tomber. Puis aussi vite que c[']est arrivé, l[']averse se calme enfin.[stopping]".
	

Part 2 - Le jeu et l[']histoire

[Instead of going nowhere: 
	say "Vous ne pouvez pas errer ainsi sans but précis à cette heure.".]

To say Description de la premiere route:
	if not visited
		begin;
			say "Un soir au nord du Portugal, sur les petites routes tortueuses de la province de Felgueiras. Votre vieille Citroën SM peine à traverser les côteaux de vignes de cette belle région viticole. Vous vous trouvez près de Celorico de Basto, et roulez en direction de Lixa, là où vous devez rencontrer votre fournisseur de vin M. Henriques, qui habite plus précisément à Vila Cova da Lixa. Représentant de commerce, vous espérez pouvoir dénicher là quelques crus de prestige pour vos clients sommeliers, votre carnet de commande est déjà plein d[']ailleurs";
		otherwise;
				say "[if player is in Citroen]Vous roulez en direction de Lixa, qui se trouve un peu plus au sud d[']ici[otherwise]Vous êtes sur la route en direction de Lixa, qui se trouve un peu plus au sud d[']ici[end if]";
		end if.
		
Petite route de montagne is a room. "[Description de la premiere route]."

The CleDouze is a thing. The printed name of cledouze is "clé de douze". Understand "cle" or "cle de douze" or "cle douze" as cledouze. The description of cledouze is "Une clé plate. La fameuse clé de douze, chère à tous les mécaniciens.".

Instead of going east when the location is Petite route descendante or the location is Petite route de montagne:
	say "C[']est beaucoup trop dangereux de descendre dans ce ravin.".

Instead of going west when the location is Petite route de montagne:
	say "La forêt est impraticable ici.".
	
The ForetFake is female scenery in Petite route de montagne. The printed name of ForetFake is "forêt". The description of ForetFake is "Vous qui aimez pourtant la nature, cette forêt ne vous rassure pas du tout.". Understand "foret" or "arbres" or "arbre" or "jungle" or "nature" as ForetFake.
	
Instead of going north when the location is Petite route de montagne:
	say "Vous n[']avez pas envie de retourner là d[']où vous venez, surtout qu[']il n[']y a rien d[']intéressant avant plusieurs dizaines de kilomètres.".
	
Instead of examining player, say "Amoureux des bons vins, vous avez choisi de devenir représentant spécialisé dans les vins blancs portugais, et vous parcourez les routes de ce pays pour dénicher les perles rares.".

Panne is a scene. Panne begins when the location is Petite route descendante for the first time. Panne ends when the courroie2 is in capot.

[TODO : Souvenir is a recurring scene. Souvenir begins when the location is Clairière. Souvenir ends when the location is not clairière. 

SouvenirFronde is a scene. SouvenirFronde begins when the time since Souvenir began is 12 minutes and Souvenir is happening. SouvenirFronde ends when panne ends.
When souvenirFronde begins, say "Ce genre de clairière vous rappelle lorsque enfant vous fabriquiez des pièges en forêt, et que vous alliez jouer là-bas avec vos amis, armés chacun d[']un lance-pierre pour dégommer des boîtes de conserves posées sur une pierre.".]

SouvenirFronde is a scene. SouvenirFronde begins when the player is in clairière for the 12th turn.
SouvenirFronde ends when panne ends.
When souvenirFronde begins, say "Ce genre de clairière vous rappelle lorsque enfant vous fabriquiez des pièges en forêt, et que vous alliez jouer là-bas avec vos amis, armés chacun d[']un lance-pierre pour dégommer des boîtes de conserves posées sur une pierre.".

[Understand the command "frotter" a something new.]

Jumping inside is an action applying to one thing.

Understand "sauter dans/sur [something]" as jumping inside.

Carry out jumping inside:
	say "Vous risqueriez de vous blesser...".

Cutting it with is an action applying to two things.
Understand "couper [something] sur/avec/contre [something]" as cutting it with.

Carry out cutting it with:
	say "Cela ne provoque rien de remarquable.".



Rubbing it on is an action applying to two things.
Understand "frotter [something] sur/avec/contre [something]" as rubbing it on.

Carry out rubbing it on:
	say "Cela ne provoque rien de remarquable."

Instead of rubbing the roue, say "Il faut préciser contre quoi vous voulez la frotter.".

Instead of rubbing the roue on something (called truc):
	if truc is coupant
		begin;
			say "Avec un peu d[']effort, car ce n[']est pas vraiment adapté à ce travail, vous réussissez à découper une tranche de caoutchouc dans cette vieille roue.";
			move courroie2 to player;
		otherwise;
			say "Cela n[']est pas assez coupant pour arriver à en découper une tranche.";
		end if.

Instead of rubbing something (called truc) on roue:
	try rubbing the roue on truc.

Instead of opening roue, say "Avec quoi ? Vos mains ?".

Instead of unlocking roue with something (called truc):
	try rubbing the roue on truc.
	
Instead of cutting roue with something (called truc):
	try rubbing the roue on truc.
	
Instead of cutting roue, try opening roue.

Instead of attacking roue, say "Si vous voulez en couper une partie, il faudra utiliser autre chose que vos seules mains.".


When panne begins:
	say "Zut, le moteur s[']arrête en s[']étouffant ! À l[']époque, cette Citroën SM, gratifiée d[']un moteur Maserati de légende, était ce qui se faisait de mieux et de plus fiable, mais depuis le temps qu[']elle donnait des signes de faiblesse, c[']était malheureusement à prévoir...".


A Citroen is a female vehicle in Petite route de montagne. The printed name of Citroen is "Citroën SM". The indefinite article of Citroen is "votre". The description of citroen is "Un capot, un coffre, un véhicule quoi. Mais à part cela c[']est une belle voiture quasi légendaire dont vous ne voudriez vous séparer pour rien au monde. Sauf quand elle vous lâche en pleine campagne peut-être.".
Understand "voiture" and "vehicule" as citroen.

Instead of taking Citroen, say "C[']est beaucoup trop lourd.".

Instead of switching on Citroen, say "[if courroie2 is not in capot]Cela ne sert à rien, il manque une pièce au moteur.[otherwise]Indiquez juste la direction souhaitée une fois que vous êtes à l[']intérieur de cette voiture.[end if]".

Instead of switching off Citroen, try switching on citroen.

Instead of pushing Citroen, say "Vous voulez que cette pièce de collection finisse dans le ravin ?".

Understand "pousser [citroen] dans [ravin]" as a mistake ("Non, cela ne serait pas une solution satisfaisante, même si cette voiture vous énerve, en plus elle ne vaut pas grand chose à l[']argus...").

Instead of pulling Citroen, try pushing Citroen.

Instead of opening citroen, say "Soyez plus précis sur la partie de la voiture à ouvrir s[']il vous plait.".

Instead of closing citroen, say "Soyez plus précis sur la partie de la voiture à fermer s[']il vous plait.".

Understand the command "conduire" as "allumer".

The carnet is undescribed in Citroen. The description of carnet is "Un carnet à la couverture de cuir. Il est important pour vous car la plupart de vos contacts sont inscrits dessus. Vous l[']aimez bien car il se prend bien en main, son contact vous rassure. Un rabat permet de bloquer sa fermeture, ce qui lui donne, dixit votre oncle Zéphyr Helianthus - ce grand scientifique -, une forme 'aérodynamique'." The printed name of carnet is "carnet de commandes". Understand "carnet de commandes" or "commandes" or "rabat" as carnet.

Instead of opening carnet, try helping. Instead of closing carnet, say "Vous faites jouer rapidement le rabat du carnet, et dans un petit claquement sec, refermez ce carnet si classe.".

Instead of reading carnet, say "Ce carnet ne sert que pour noter les commandes de vos clients français, ici vous n[']avez pas spécialement besoin de lire la liste de tout cela.".

Understand "feuilleter [carnet]" as a mistake ("Vous parcourez quelques pages intérieures, mais rien ne vient attirer spécialement votre attention.").

The chandail is undescribed wearable in Citroen. The description of chandail is "Un pull marron et chaud. Vous pourriez en avoir besoin si la nuit s[']annonçait trop froide...". Understand "pull" or "pull over" as chandail.

After dropping chandail when the grenouille is in location:
	now grenouille is tamed;
	say "Vous posez votre habit au sol. C[']est alors que la grenouille s[']approche de ce chandail et se roule dedans. Elle semblait avoir froid et votre pull lui a donné un peu de réconfort."

Instead of wearing chandail when the grenouille is tamed, say "Beurk, n[']oubliez pas que la grenouille s[']est roulée dedans !".

Instead of inserting something (called truc) into chandail:
	say "Ce chandail est trop mou pour pouvoir contenir quoi que ce soit, de toute façon il est à votre taille, pas à celle de ce truc.".

Instead of inserting grenouille into chandail:
	if grenouille is not tamed, say "Encore faudrait-il pouvoir l[']attraper.";
	if grenouille is tamed, say "Elle doit avoir assez chaud maintenant.";

The coffre is an openable closed container. It is part of the Citroen. Instead of opening coffre when the player is inside citroen, say "Vous ne pouvez pas ouvrir le coffre depuis l[']intérieur de votre véhicule.".

The capot is an openable closed container. It is part of the Citroen. Instead of opening capot when the player is inside citroen, say "Vous ne pouvez pas ouvrir le capot depuis l[']intérieur de votre véhicule.". [Understand "moteur" as capot.]

The moteur is in capot. The moteur is fixed in place.


Some portes are part of the citroen.

Understand "porte" or "portiere" or "portieres" as portes.

Instead of opening portes, say "Il vous suffit d[']entrer ou de sortir du véhicule pour que cette action soit automatique.".

Instead of closing portes, try opening portes.


Instead of opening capot when the player is not inside citroen:
	say "Vous ouvrez le capot[if panne is happening], et la panne est facile à détecter : une courroie de transmission qui était usée et s[']est rompue[end if].";
	now the capot is open.

The courroie is a female thing in capot. The description of courroie is "Elle était bien vieille cette courroie en caoutchouc. En tout cas, si elle s[']est rompue, le reste semble encore suffisamment souple et résistant.". Understand "vieille" or "vieille courroie" or "courroie de transmission" as courroie. The printed name of courroie is "vieille courroie".

The courroie2 is a female thing. The description of courroie2 is "Une courroie presque neuve, fabriquée par vos soins de façon quasi-artisanale.". Understand "courroie" or "neuve" or "nouvelle" or "fabriquee" or "tranche" or "de fortune" as courroie2. The printed name of courroie2 is "courroie de fortune".

Instead of inserting the courroie2 into capot:
	say "[if courroie2 is inside capot]Mais elle y est déjà ![otherwise]Vous replacez la nouvelle courroie dans le moteur.[end if]";
	move courroie2 to capot.
	
Before inserting the courroie2 into capot for the first time:
	award 1 point;
	continue the action.
	
The bosse is female thing. The bosse is scenery. The bosse is part of the capot. The description of bosse is "Pas grave, de petits accrochages, il y en a toujours sur une voiture.".

The lance-pierre is an open container. It is fixed in place. The description of lance-pierre is "Une catapulte rudimentaire, formée par une courroie attachée entre deux poteaux appairés. La courroie n[']était pas très élastique, mais restait souple et vous devriez pouvoir l[']utiliser comme un lance-pierre.". Understand "catapulte" or "fronde" or "lance-pierre" or "lance pierre" as lance-pierre.

Rule for printing the name of the lance-pierre:
	say "lance-pierre";
	omit contents in listing.

Instead of tying courroie to torches:
	say "[if lance-pierre is in location]C[']est déjà fait ![otherwise]Nouant une extrémité de la courroie rompue à un des poteaux, et l[']autre extrémité à celui juste à côté, vous fabriquez une sorte de gros lance-pierre.[end if]";
	move courroie to location;
	move lance-pierre to location.

Instead of entering lance-pierre, say "Vous n[']irez pas loin ni très haut avec ça...".

Before tying courroie to torches for the first time:
	award 1 point;
	continue the action.

After taking courroie when the location is clairière:
	 remove lance-pierre from play;
	continue the action.

Instead of inserting something into lance-pierre, say "Vous n[']arriverez à rien avec ce type d[']objet.".

Instead of inserting something (called truc) into courroie when lance-pierre is in location, try inserting truc into lance-pierre.

Instead of inserting something (called truc) into courroie when lance-pierre is not in location, say "Une fronde ? Une catapulte ? Voilà une bonne idée. Mais vous ne pouvez utiliser cette courroie seule si vous voulez obtenir une catapulte puissante.".

Instead of inserting bouteille into lance-pierre, say "Trop dangereux, cela risquerait de voler en éclat. De plus si l[']arôme de ce vin est bien équilibré, sa bouteille ne l[']est pas.".

Instead of inserting carnet into lance-pierre for the first time:
	say "Vous fermez le rabat du carnet pour qu[']il ne s[']ouvre pas, et placez votre précieux carnet de commandes dans la courroie. Vous visez la roue, et relâchez la courroie. Coup de chance, le frêle projectile réussit à venir frapper la roue en équilibre, et la fait tomber au sol. Malheureusement, il reste quand à lui au sommet de la pierre noire, à jamais inaccessible.";
	move roue to location;
	move carnet to pierre noire;
	now carnet is not reachable.

Instead of inserting grenouille into lance-pierre:
	if roue is on pierre noire
		begin;
			say "Vous placez la grenouille, qui se laisse faire bon gré, mal gré, dans la courroie. Elle ne semble pas s[']affoler tandis que vous visez la roue, et relâchez la courroie. L[']animal roule dans les airs, visiblement ravi, et vient frapper avec ses pattes arrière la roue en équilibre, faisant tomber cette dernière au sol, et la remplaçant de ce fait. Ce sanctuaire a retrouvé son dieu !";
			move roue to location;
			now roue is reachable;
			move grenouille to pierre noire;
			now grenouille is not reachable;
		otherwise;
			say "[if carnet is on pierre noire or roue is in location][otherwise]Laissez donc cette pauvre grenouille tranquille ![end if]";
	end if;
	if carnet is on pierre noire
		begin;
			say "Vous placez la grenouille dans la courroie, qui se laisse faire bon gré, mal gré. Elle ne semble pas s[']affoler tandis que vous visez la roue, et relâchez la courroie. L[']animal roule dans les airs, visiblement ravi, et va atterrir juste sur le sommet de la pierre. Il pousse ensuite de ses petites pattes votre carnet, qui retombe au sol. Ce sanctuaire a retrouvé son dieu !";
			move carnet to location;
			now carnet is reachable;
			move grenouille to pierre noire;
			now grenouille is not reachable;
		otherwise;
			say "[if carnet is on pierre noire or roue is in location][otherwise]Laissez donc cette pauvre grenouille tranquille ![end if]";
	end if.

The bouteille is female in coffre. The description of bouteille is "Cette bouteille est la dernière qui vous reste de cette excellente année de la récolte de M. Henriques.".
Instead of reading bouteille, say "Vinho Verde - Denominação de Origem Controlada - António José Henriques - 1987.".
Understand "bouteille de vin" or "etiquette" as bouteille.

Instead of drinking bouteille when the player is in Petite route de montagne, say "Boire ou conduire, il faut choisir...".

The tesson is a thing. The tesson is coupant.

Instead of attacking bouteille:
	say "Vous cassez la bouteille sur le sol, et il ne vous reste plus qu[']un tesson coupant. Quel gâchi, un aussi bon vin.";
	remove bouteille from play;
	now the player is holding tesson.

[Before drinking bouteille when the player is in Foret:
	say "L[']ambiance de cette forêt vous coupe toute envie de boire.";
	stop the action.]

Instead of drinking bouteille for the first time:
	say "Bonne idée, au moins cette journée ne sera pas complètement perdue. Vous en prenez une gorgée.".

Instead of drinking bouteille for the second time:
	say "Vous en buvez une rasade. De toute façon, il vous restera toujours l[']étiquette pour la référence à demander à votre fournisseur.".
	
Instead of drinking bouteille for the third time:
	say "Hmm, quelle bonne bouteille. Vous avez du mal à garder le contrôle de vos gestes après avoir bu le contenu complet de la bouteille, et elle vous échappe des mains pour aller rouler [if the player is in foret]sous une racine[otherwise]dans un ravin en bas[end if].";
	say "[If location is clairière]Vous voyez alors à votre grand étonnement une procession entrer dans la clairière, depuis les profondeurs de la forêt. Ce sont des hommes et des femmes masqués, revêtus de longues toges et manteaux.[end if]";
	remove bouteille from play;
	move cultistes to clairière.


Petite route descendante is south from Petite route de montagne. "[if not visited]La route oblique maintenant doucement vers le sud, et après avoir traversé une partie du domaine de M. Henriques, domaine constitué de vignes encore en fleurs à cette époque de l[']année, elle descend maintenant franchement vers la ville, où vous espérez pouvoir bientôt le retrouver. Vous n[']avez pas le temps de passer une petite forêt que votre voiture fait un bruit bizarre...[otherwise]À flanc de coteaux vers la plaine de Lixa. La route continue vers le sud, et une forêt se trouve à l[']ouest. Sur votre gauche, à l[']est, se trouve un ravin.".

Petit chemin vers la forêt is west from Petite route descendante. "Il y a une ambiance inquiétante ici, mais on dirait que vous n[']avez pas trop le choix. Le chemin s[']ouvre vers une clairière illuminée à l[']ouest. Votre esprit est tellement angoissé par cette atmosphère oppressante, que vous faites abstraction de tout le reste de la végétation.".
[TODO]

Instead of listening when the location is Petit chemin vers la forêt, say "Des bruits inquiétants et indéfinissables hantent l[']atmosphère.".

Route vers le sud is south from Petite route descendante. 

Before going to Petite route descendante when panne is not happening and the player is not inside citroen and citroen is in location:
	say "Cela ferait beaucoup trop long d[']espérer descendre tout ce chemin à pied.";
	stop the action.
	
Before going to Route vers le sud when panne is happening:
	say "Votre voiture est inutilisable et cela ferait beaucoup trop long d[']espérer descendre tout ce chemin à pied.";
	stop the action.

Before going to Route vers le sud:
	if courroie2 is not in capot
		begin;
			say "La voiture ne pourra pas démarrer sans sa courroie de transmission, ou quelque chose d[']équivalent qui en fera office.";
			stop the action;
		otherwise;
			continue the action;
	end if.

After going to Route vers le sud:
	if player is holding carnet or carnet is inside citroen or carnet is inside coffre, award 1 point;
	if the player is holding bouteille or bouteille is inside citroen or bouteille is inside coffre, award 1 point;
	say "Vous reprennez enfin votre route, un peu étonné par tout ce que vous avez pu découvrir cette nuit. Quoi qu[']il en soit, vous arriverez bientôt au domaine de M. Henriques, et vous aurez l[']esprit occupé par des choses bien plus agréables !";
	if the score is 4
		begin;
			say "Votre carnet de commandes plein à craquer, et le cru du siècle comme laissez-passer pour en obtenir d[']autres, vous allez vraiment faire un malheur à votre retour.";
			end the game in victory;
		otherwise;
			say "Dommage que vous ayez perdu quelques affaires importantes durant cette soirée chaotique...";
			end the game saying "Mais le plus important c[']est quand même que vous vous en soyez sorti.";
		end if.

Before going somewhere when the player is inside citroen:
	if panne is happening
		begin;
			say "Votre véhicule est inutilisable à l[']heure actuelle.";
			stop the action;
		otherwise;
			say "Vous continuez la route au volant de votre voiture.";
			continue the action;
		end if.

Before going to Petit chemin vers la forêt when the player is inside citroen, say "Ce chemin est impraticable avec une voiture.".

The grenouille is a female animal in Petit Chemin vers la forêt. The description of grenouille is "Tiens, cela ressemble à une Heleophryne Natalensis, comme aurait pu dire votre oncle Zéphyr Helianthus, qui travaille au musée d[']histoire naturelle.[if grenouille is untamed] Le pauvre animal semble terrorisé, car il tremble de partout.[end if]".
Understand "animal" or "batracien" or "Heleophryne natalensis" as grenouille.

Instead of talking to grenouille, say "Croak, croak..., savez-vous réellement parler grenouille ? En tout cas la belle reste de glace.".

Instead of smelling grenouille, say "Elle sent probablement un peu la vase, mais vous n[']avez pas spécialement envie d[']en savoir plus.".

Instead of taking grenouille:
	if grenouille is untamed
		begin;
		say "Elle se sauve à votre approche, en poussant un coassement chevrotant, mais néanmoins effaré.";
		move grenouille to Clairière;
	otherwise;
		if grenouille is on pierre noire, continue the action;
		say "Vous la prenez sans difficulté entre vos mains.";
		now the player is holding grenouille;
	end if.
	
Instead of attacking or touching or pushing grenouille, try taking grenouille.


Instead of kissing grenouille, say "Ce genre de chose, cela ne fonctionne que dans les contes de fée, pas dans les fictions interactives mettant en scène un simple représentant de commerce !".

Instead of showing chandail to grenouille, say "La grenouille vous regarde avec deux gros yeux ronds.".

Instead of throwing something (called projectile) at grenouille:
	move projectile to location;
	say "Pas assez rapide ! Le batracien s[']est échappé avant de recevoir [the projectile] sur la tête.".

Instead of giving chandail to grenouille, try dropping chandail.

Clairière is west from Petit chemin vers la forêt. The description of clairière is "En arrivant dans la clairière, vous voyez qu[']une pierre étrange se dresse en son centre.". 

Instead of going nowhere when the location is clairière: 
	say "La forêt est trop dense et trop sombre pour espérer pouvoir aller dans cette direction.".

The roue is a female thing. The roue is undescribed. The description of roue is "Une roue de voiture. Un pneu en caoutchouc quoi.". Understand "pneu" as roue.

Understand "souffler sur [roue]" as a mistake ("Si votre souffle pouvait faire bouger ce pneu, le vent l[']aurait déjà fait avant vous.").

Instead of taking a thing (called truc):
	if truc is on pierre noire
		begin;
			say "La pierre est trop haute, et ce qui est dessus n[']est pas accessible.";
			stop the action;
		otherwise;
			continue the action;
		end if.		

Instead of putting something on pierre noire, say "La pierre est trop haute pour que vous y posiez simplement quelque chose dessus.".

Instead of throwing something (called truc) at pierre noire:
	move truc to location;
	say "Vous lancez [the truc], mais le sommet de la pierre est trop loin pour que vous puissiez espérer y poser quelque chose dessus avec précision si vous utilisez la seule force de vos poignets.".

Instead of throwing something (called truc) at roue when roue is on pierre noire:
	move truc to location;
	say "Vous lancez [the truc] vers la roue, mais vous manquez de précision si vous utilisez la seule force de vos poignets. Il vous faudrait quelque chose de plus puissant pour propulser [the truc] et espérer faire tomber ce pneu de la pierre noire.".

The pierre noire is female supporter inside Clairière. The pierre noire is fixed in place and coupant. The description of pierre noire is "Cette pierre gravée, haute de plusieurs mètres et qui semble très ancienne, représente vraisemblablement en bas-relief le dieu antique Baal, dans son avatar à tête de crapeau. Votre oncle Zéphyr, chercheur au musée d[']histoire naturelle de Bordeaux, serait aux anges, enfin, façon de parler. Cinq groupes de deux torches fixées sur des pieux en métal encore plus haut que le mégalithe, éclairent de part et d[']autre cette pierre, en faisant luire certains de ses angles aiguisés comme des rasoirs[if roue is on pierre noire]. Une roue de voiture se trouve en équilibre de façon insolite en haut de cette pierre[end if].".
Understand "pierre gravee" or "baal" or "dieu antique" or "dieu" or "bas-relief" or "megalithe" or "angle" or "angles" or "angles aiguises" as pierre noire.
	

Instead of attacking pierre noire, say "Vous ne faite pas bouger d[']un pouce cette roche massive.".

Instead of pushing or pulling or waving pierre noire, try attacking pierre noire.

Instead of climbing pierre noire, say "Il n[']est pas possible de l[']escalader, de plus vous risqueriez de vous couper sur les angles vifs qui ont été taillés dedans.".

Some torches are female inside Clairière. The torches are fixed in place. The torches are not reachable. The description of torches is "Des torches fixées sur des pieux en métal éclairent de part et d[']autre la pierre. Les pieux sont plantés par paires dans le sol, et ils sont espacés d[']une cinquantaine de centimètres au sein d[']une même paire.".
Understand "pieu" or "pieux" or "pieux en metal" or "poteau" or "poteaux" or "lumiere" or "torche" as torches.

Instead of switching off torches, say "Elles sont placées trop haut pour pouvoir les atteindre.".


Instead of climbing torches, say "Les poteaux en métal sont trop glissants pour espérer pouvoir y monter. Peut-être que si vous n[']aviez pas abusé du 'feijoada à transmontana' lors du repas de midi et repris ensuite trois fois du 'pasteis de nata' en dessert, vous auriez eu une petite chance, mais là, impossible.".

Some cultistes are men. The description of cultistes is "Habillés de long manteaux rouges, avec des masques grotesques à plume, ils se tiennent en cercle autour de la pierre noire. Sont-ils réellement là, ou bien le rêvez-vous ? Les vapeurs de l[']alcool obscurscissent encore un peu votre esprit.".
Understand "homme" or "hommes" or "femme" or "femmes" or "groupe" or "gens" or "manteaux" or "toges" or "procession" or "idolatres de la pierre noire" or "idolatres" or "idolatre" as cultistes. The printed name of cultistes is "idolâtres de la pierre noire". 

Instead of attacking cultistes:
	say "Leurs contours disparaissent au moment où vous allez sur eux.";
	remove cultistes from play.

Instead of talking to cultistes: 
	say "[if roue is on pierre noire]Vous leur criez que vous avez un problème avec votre voiture, un truc tout bête, une courroie de cassée et la voiture ne veut plus démarrer. Auraient-ils une solution, malgré l[']absurdité de la situation ?[otherwise]Vous essayez d[']entamer la conversation, mais vous semblez plutôt les déranger.[end if]";
	say "L[']un d[']eux vous fait signe de vous taire avec de grands gestes, et vous montre le sommet de la pierre noire. Ensuite, ils semblent tous se dématérialiser. Les effets de l[']alcool se dissolvent peut-être également et vous reprenez vos esprits.";
	remove cultistes from play.

Instead of showing courroie to cultistes, try talking to cultistes.

When play begins:
	move player to Citroen;
	move roue to pierre noire;
	now roue is not reachable.
	

Foret is a region. Petit chemin vers la forêt and Clairière are in Foret.

BordureDeRoute is a region. Petite route de montagne, Petite route descendante and Route vers le sud are in BordureDeRoute.

The ravin is a backdrop in Petite route de montagne and in Petite route descendante. The description of ravin is "Vous n[']en voyez pas trop le fond à cette heure de la journée, de toute façon cela ne semble pas très praticable.".

Instead of throwing something (called projectile) at ravin:
	remove projectile from play;
	say "Voilà, vous êtes maintenant débarrassé de cet objet ([the projectile]) pour toujours.".
	
instead of inserting something (called projectile) into ravin, try throwing projectile at ravin.

The ravin is not reachable.

Some racines are backdrop in foret. Understand "racine" as racines. Instead of searching racines, say "Vous n[']avez plus l[']esprit assez clair pour trouver quoi que ce soit à cette heure de la nuit.".

Some branches are backdrop in foret. Understand "branche" or "vegetation" or "arbres" or "foret" or "arbre" as branches. Instead of taking branches, say "Vous ne trouvez pas de branche assez longue pour être utilisable.".

Some rochers are backdrop in BordureDeRoute. Understand "rocher" or "roches" or "caillou" or "cailloux" as rochers. Instead of taking rochers, say "Vous ne trouvez pas de rocher assez petit pour être utilisable, il n[']y a ici que de gros rochers.".

Some vignes are backdrop in BordureDeRoute. Understand "vigne" or "raisin" or "raisins" as vignes. Instead of doing anything to vignes, say "Les vignes s[']étendent au loin vers l[']est, au delà du ravin.".



Test me1 with "sud/x carnet de commandes/prendre le carnet/sortir/prendre le chandail/ouvrir le capot/prendre la courroie/ouvrir le coffre/prendre la bouteille/ouest/prendre la grenouille/ouest/poser le pull/prendre la grenouille/attacher la courroie aux torches".

Test me2 with "mettre la grenouille dans la catapulte".
Test me2b with "mettre le carnet dans la catapulte".

Test me3 with "prendre la roue/decouper la roue avec un angle/est/est/mettre la courroie de fortune dans le capot".

Test me with "test me1/test me2/test me3".
	
	