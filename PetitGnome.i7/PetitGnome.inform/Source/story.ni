"Petit gnome" by Otto Grimwald.  

[bienvenue sur cette présentation rapide d'Inform 7. Ce petit exemple devrait vous permettre de démarrer rapidement l'apprentissage du système Inform 7, mais il ne remplacera pas la lecture attentive de la documentation complète.]

[Vous noterez que tous les commentaires se font entre crochets. De plus, sauf à quelques exceptions, l'ordre des déclarations n'a pas d'importance pour Inform. Si à la compilation il y a un problème au passage 'include (- verb ... -)', veuillez rétablir la syntaxe correcte, c'est lié à l'importation...]


Part 1 - Quelques définitions pour le jeu

[Il est utile de subdiviser le code en "part, chapter, section" pour améliorer la lisibilité]

The story genre is "Comédie".    [style du jeu]
The story creation year is 2006.  [année de création du jeu]
The story headline is "Une aventure dans le jardin". [un sous-titre, une accroche pour votre jeu...]
The story description is "Un soir d'hiver, dans un coin perdu du monde. Il s'agit d'une petite histoire très simple, ayant pour but de présenter et développer la fiction interactive en français avec Inform 7. Si vous voulez développer des points de cette histoire, merci de m'en faire part sur le forum du site http://ifiction.free.fr ".  [la description du jeu]

[licence de cet exemple : CC-BY-SA]  [inform 7 ne prend pas encore en compte l'inclusion de licence de manière officielle]

Release along with the source text, cover art, a website.[, a file of "version z5" called "PetitGnome.z5", a file of "version glulx" called "PetitGnome.blb", a file of "solution" called "solution2.txt",  a file of "solution (autre version)" called "solution.txt", a file of "petitgnomecopie.tgz (pour tests)" called "petitgnomecopie.tgz", the introductory booklet. ] [Ce qui précède permet de générer un site internet avec éventuellement des fichiers additionnels qui apparaîtrons sur ce site]

Include French by Eric Forgeot.            [à inclure obligatoirement pour obtenir le support du français]
[Include Boussole by Benjamin Roux.]  [si vous l'avez dans vos extensions, vous pouvez décommenter cette inclusion.Vous pouvez également inclure toute extension qui vous sera nécessaire en utilisant cette syntaxe.]

Use French Language.                      [à inclure obligatoirement pour sélectionner la bonne version de la langue (présent, passé etc)]

Figure of gnome is the file "gnome.png".  [pour rajouter des images, à mettre dans le dossier "figures" du dossier "materials" du jeu. Si les images ne sont pas là, elles ne seront pas prises en compte de toute façon.]
Figure of titre is the file "titre.png".

Use full-length room descriptions. [permet d'afficher la description complète des lieux à chaque visite]

After printing the name of a container: omit contents in listing.  [évite de préciser lorsqu'un contenant est vide ou plein]

The carrying capacity of the player is always 4.  [défini le nombre d'objets transportables simultanément dans les mains du joueur]

The maximum score is 4.  [comme son nom l'indique, défini le score maximal]

Part 2 - Le monde du jeu, avec lieux et objets

Chapter 1 - A l'extérieur

Section 1 - Les lieux

Dehors is a region. 
SurLeToit, Jardin, Jardin2 and Maison are in Dehors. [les régions permettent de regrouper les lieux en utilisant des classes à prédéfinir, chacune ayant éventuellement ses caractéristiques propres.]

East of Jardin is Jardin2. Above is SurLeToit.   [les lieux sont définis en fonctions des autres et en utilisant les points cardinaux.]

SurLeToit is a room. The printed name of SurLeToit is "Sur le toit". "Après avoir effectué votre travail sur l'ensemble des bâtisses de cette petite ville provinciale, vous vous trouvez enfin sur le toit de la dernière des maisons à visiter. Quel travail..."
[pour une description qui suit immédiatement la définition d'un lieu ou d'un objet, il est possible de se passer de la formule complète : 'the description of ... is ...']
[The 'printed name of ... is ...' permet d'assigner le nom qui sera affiché pour un lieu ou un objet, au lieu d'utiliser le nom de l'objet inform. Il aurait été possible d'utiliser la formulation 'Sur le toit is a room', mais cela aurait été peut-être plus long à terme pour répéter ce nom tout le long du jeu, et parfois cette formulation peut causer des conflits.]

Before looking in SurLeToit for the first time, display the Figure of titre.  [affiche éventuellement une image, si elle est disponible.]

After going in SurLeToit:
	if Jardin2 is not visited
	begin;
		say "Les montagnes au loin sont déjà auréolées des lueurs de l'aurore, vous êtes impatient de finir ce travail et de rentrer chez vous."; continue the action;
	Otherwise;
		say  "Vos vieux os supporteront-ils toutes ces allées et venues ?"; continue the action;
	end if;
[Ceci est une règle qui définit ce qui se passe après être remonté sur le toit, et cela teste également si un autre lieu a déjà été visité ou pas en donnant des réponses différentes suivant le contexte.]

Section 2 - Les objets 

Instead of examining player, say "Vous n[']avez pas de rétroviseur sur le traineau pour vous admirer.".   [Permet de personnaliser la description du joueur lorsqu'il essaye de se regarder.]

The cheminée is in SurLeToit. Cheminée is a container. Cheminée is female and fixed in place. Understand "cheminee" as cheminée. The description of cheminée is "Les cheminées, ça vous les connaissez ![if feu is switched on]On dirait qu'il y a du feu en dessous.[end if]" 

The traineau is a vehicle in SurLeToit. Understand "traineau/rennes/chariot/cariole" as traineau.

Instead of going to salon when the player is in a vehicle, say "Vous ne pouvez entrer ici avec votre traineau.".

Hotte is a player's holdall. The player is wearing hotte. Hotte is female. The printed name of hotte is "la hotte".   [définition d'un "sac" pour le joueur, qui pourra contenir plus d'objets que la limite imposée plus haut]

The cadeau is a thing. Understand "paquet" or "jouet" as cadeau.

The carte is a female thing. Understand "carte de visite" or "cartes" or "cartes de visite" or "pile" as carte. The printed name of carte is "carte de visite". The description of carte is "Une pile de cartes de visite à votre effigie que vous laissez généralement avec les cadeaux que vous livrez.".
Instead of reading carte, say "'Avec les compliments de Papa Noël !'".

Instead of showing carte to gnome, try giving carte to gnome.

Instead of giving carte to gnome for the first time:
	say "Il prend la carte, et sursaute aussitôt.";
	say "Après avoir fait une petite révérence, il dépose sur le sol une clé, et se dirige vers la porte de la maison.";
	move gnome to jardin;
	move GnomeCle to location of the player.
	

Instead of giving cadeau to gnome, say "Il n'est veut pas. Il vous explique qu'il s'est déjà échappé d'un endroit où il devait en empaqueter des centaines par jour, aussi il n'aime plus ces choses. D'ailleurs en vous regardant il semble un peu vous craindre.".


Instead of inserting cadeau into cheminée:
	if chaussons is on chenets
		begin; 	
			award 2 points;
			say "Vous entendez le bruit feutré du paquet qui tombe dans les chaussons. Et un heureux de plus !";		
			move cadeau to chaussons;
			stop the action;
		otherwise;
			say "Vous ne pouvez pas, il vous semble qu'il y a un problème pour livrer ce cadeau actuellement. Il vaudrait mieux investiguer un peu plus.[if feu is switched on]D'ailleurs on dirait qu'il y a du feu en dessous.[end if]";
			stop the action;		
	end if;
	
Instead of inserting something which is not cadeau into cheminée, say "Vous ne pouvez pas non plus y mettre n'importe quoi.".


Jardin is a room. The printed name of Jardin is "Un beau jardin". "Ce petit jardin propret vous fait penser au vôtre. Sans doute qu'il y a là quelques gnomes agiles qui viennent l'entretenir. Il continue à s[']étendre, sur votre droite, à l'est. La maison se trouve au sud."

Instead of examining Jardin for the second time, say "Vous êtes dans le jardin, tout près de la maison. Vous pouvez remonter sur le toit depuis ici."

Jardin2 is a room. The printed name of Jardin2 is "Le bout du jardin". "Vous êtes près de la limite de la propriété. Au delà s'étendent les autres jardins du voisinage."


After going in Jardin2:
	if Jardin2 is not visited
	begin;
		display the Figure of gnome;
		say ""; continue the action;
	Otherwise;
		say  "" ; continue the action; 
	end if;
		
The gnome is in jardin2.  
The gnome is a man. Understand  "gnome/troll/creature/lutin" or "petit gnome" as Gnome. The description of Gnome is "C'est un lutin à l'air espiègle. Vous vous demandez si ce n'est pas lui qui garde la maison en l'absence de ses propriétaires.[if gnome is carrying chaussons] Il garde à la main une paire de chaussons.[end if]".

Some chaussons are a container. The printed name of chaussons is "deux chaussons". Understand "chausson" or "deux chaussons" or "paire" or "paire de chaussons" or "chaussures" as chaussons.


GnomeCle is a thing. The printed name of GnomeCle is "clef". Understand "clé/clef/cle" as gnomecle.

[Instead of asking the Gnome about something, say "Vous parlez le langage gnome, mais celui-ci est resté si longtemps auprès des humains qu'il ne le connaît plus lui-même."]


Instead of attacking Gnome:
	say "Il prend peur et se sauve en courant, laissant derrière lui une clé et une paire de chaussons.";
	remove gnome from play;
	move GnomeCle to location of the player;
	move Chaussons to location of the player;

Instead of asking Gnome for chaussons:
	say "Il regarde ses vieux chaussons à contrecoeur, et il vous les tend.";
	move chaussons to location.

The PorteMaison is a door. It is south of Jardin and north of Salon. Salon is inside from Jardin. PorteMaison is female, locked and closed. The matching key of the PorteMaison is the GnomeCle. The printed name of PorteMaison is "porte d'entrée". The description of PorteMaison is "La porte de la maison est en acier qui semble épais." 
Understand "portail/porte/seuil/entree" as PorteMaison.

[Instead of going to salon when the player is in Jardin:
	if PorteMaison is closed,
		say "Vous devez ouvrir la porte avant d'entrer.";
	Otherwise continue the action.]

Instead of going to salon when the player is in Jardin: 
	if PorteMaison is not locked
		begin;
			try opening PorteMaison;
			continue the action;
		Otherwise;
			say "La porte est verrouillée.";
	end if.

Instead of pushing or pulling or attacking the PorteMaison for the first time:
	say "Vous frapper vigoureusement à la porte, manquant de la faire céder sous vos coups. Mais on dirait qu'il n'y a personne pour répondre.".

Every turn when in Jardin:
	if PorteMaison is closed
	begin;
		if a random chance of 15 in 100 succeeds 
		begin;
			say "Ne pensez-vous pas que vous devriez essayer d'entrer ?";	
		otherwise;
			say "";
		end if;
	end if.


Instead of pushing or pulling or attacking the door for the second time, say "Voudriez-vous vous reconvertir dans le braquage ? Qu'en penserait votre femme ?"

Instead of pushing or pulling or attacking the door for the third time:
	say "Vous voyez une voiture de police qui passe par là. Des policiers vous arrêtent et vous mettent directement en prison.";
	end the game saying "Vous finissez la nuit au poste de police, vous n'avez pas pu distribuer vos cadeaux. Les enfants vont être déçus !".


Chapter 2 - A l'intérieur

Section 1 - Les lieux

Dedans is a region. Salon is in dedans. La cuisine is in dedans.

Salon is a room.
The description of Salon is "C'est un joli salon. [if Salon is unvisited] Vous voyez la cheminée au fond de la pièce, un feu joyeux y brûle. [end if]Il y a la cuisine à l[']ouest d[']ici."

La cuisine is west from Salon. The description of La cuisine is "Cette cuisine est presque vide. Le salon se trouve à l[']est d[']ici.".

Section 2 - Les objets 

The chaise is female supporter in La cuisine. The chaise is enterable.
The pain is edible in La cuisine.

In salon are chenets. Chenets are a supporter. The description of chenets is "Les chenets de la cheminée". The printed name of chenets is "des chenets". Understand "cheminée/hâtre/chenets" as chenets.

The feu is a switched on device in salon. It is fixed in place. The description of feu is "Ce feu est là pour chauffer la pièce, mais il vous empêche de faire correctement votre travail."

A vase is in salon. The printed name of vase is "vase". Understand "eau/l'eau/de l'eau/vase" as vase.

Instead of switching off feu :
	if player has vase
	begin;
		say "Vous utilisez l'eau du vase pour éteindre le feu.";
		award 1 point;
		continue  the action; 
	Otherwise;
		say "Avec quoi voulez-vous éteindre ce feu ?";
		stop the action;
	end if;

[Understand "éteindre le feu avec le vase" as switching off.]
	
Instead of switching on feu, say "Vous ne pouvez pas le rallumer."

Before putting chaussons on chenets :
	if feu is switched off
	begin;
		award 1 point;
		say "Ils ont dû oublier de le faire. Heureusement que vous êtes consciencieux et que vous le faite à leur place."; 
	Otherwise; 
		say "Le feu brûlerait ces chaussons";
		stop the action;
	end if;
	
Part 4 - nouveaux verbes


Understand "effrayer [someone]" as attacking. [création d'un nouveau synonyme]
[d'autres définitions de verbes sont réalisées en Inform 6, voir plus bas]

Understand "aide" as a mistake ("Veuillez taper vos instructions avec des mots simples. Par exemple 'descendre', 'regarder dans la cheminee' etc...").  [Permet de définir rapidement une commande, qui n'existe pas déjà dans le dictionnaire des verbes, et d'afficher un message par rapport à cela.]


Mendier is an action applying to nothing.
Understand "mendier" as mendier.

Check mendier:
	if location is in dehors, say "Vous ne pouvez vous résoudre à faire cela ici." instead.
Carry out mendier:
	say "Vous ne voyez pas l'intérêt de faire cela ici."


[parler]

A person has a table-name called conversation. 

The conversation of gnome is the Table of gnome Conversation.

Instead of asking someone about something:
	let the source be the conversation of the noun;
	if topic understood is a topic listed in source
	begin;
		if there is a cle entry
		begin;
			say "";
		otherwise;
			say "";
		end if;
		if there is a turn_stamp entry
		begin;
			say "[The noun] vous a dit que [summary entry][paragraph break]";
		otherwise;
			change turn_stamp entry to the turn count;
			say "[reply entry][paragraph break]";
		end if;
	otherwise;
		say "[The noun] vous regarde sans comprendre. Vous savez parler le langage gnome, mais celui-ci est resté si longtemps auprès des humains qu'il ne le connaît plus bien lui-même. Essayez de le questionner autrement ou sur un autre sujet.";
	end if. 


Understand "parler a [someone]" as chatting.
Understand "parler au [someone]" as chatting.

Understand "parler avec [someone]" as chatting.

[Understand "parler à [someone]" as a mistake ("Utilisez 'questionner' quelqu'un au sujet de quelque chose.").]


Understand "talk to [someone]" as a mistake ("Utilisez 'questionner' quelqu'un au sujet de quelque chose.").

Understand "talk [someone]" as chatting.
[Understand "parler avec [someone]" as chatting.]


[Understand the command "parler" as talk.]

Understand the command "bavarder" as talk.
[Understand the command "questionner" as parler avec.]


[Understand the command "parler avec/parler a" as talk.]


chatting is an action applying to one thing. 

Carry out chatting:
	say "Vous ne savez pas quoi dire."



Table of Gnome Conversation
libelle	topic	reply	summary	cle	turn_stamp
"la maison"	"la maison/batisse" or "demeure" or "maison"	" 'Aye, je suis le gardien de la maison.' "	"il était le gardien de cette maison."	1	a number
"pour entrer"	"rentrer/entrer/entrée/entree"	"Il secoue sa tête : 'Nan, personne ne peut rentrer ici, sauf les propriétaires et ceux qu'ils invitent.'"	"il refuse de laisser entrer les étrangers à cette maison."			


Instead of chatting Gnome :
	say "Vous pouvez questionner [the noun] à propos de ces sujets : ";
	let the source be the conversation of the noun;
	repeat with N running from 1 to the number of rows in the conversation
	begin;
	say " [libelle in row N of the table of Gnome Conversation], ";
	end repeat;
	say " et vous ne voyez pas trop quoi d'autre pour le moment.[paragraph break]".

Include (-

Verb 'faire' 
	* 'peur'  '->'/'a'/'au' creature -> Attack 
	* 'peur' creature -> Attack 
	* 'peur' 'a'/'au' creature -> Attack; 
	
-)

[vous pouvez inclure du code pour inform 6 de cette manière...]

Part 3 - Les scènes

Arrivee is a scene. Arrivee begins when the player is in SurLeToit for the first turn. Arrivee ends when the player is in Salon for the first turn.

[Thinking is being pensif.
Understand the command "penser [text]" as think.]

Instead of thinking during arrivee, say "Vous sentez que vous avez des problèmes avec cette maison."

Instead of thinking in SurLeToit, say "Depuis des siècles et des siècles, en plein coeur de l'hiver, vous devez aller de maison en maison pour délivrer des présents aux enfants. C'est une activité qui vous a toujours plus, même si ce n'est pas toujours facile de le faire avec tout ce que l'on voit de nos jours..."


FinDuJeu is a recurring scene. FinDuJeu begins when the player is in a vehicle. FinDuJeu ends when the player is in SurLeToit.

When FinDuJeu begins:
	if cadeau is inside chaussons or cadeau is on chenets
		begin;
			say "Vous avez enfin réussi à terminer votre tournée de cette année. Il est temps de rentrer chez vous.";
			end the game in victory;
		otherwise;
			say "Vous ne pouvez vraiment pas partir ainsi en ne terminant pas correctement votre travail.";
	end if.


When play begins:
	move chaussons to gnome;
	move hotte to player;
	move cadeau to hotte;
	move carte to hotte.  [permet d'initialiser des données au début du jeu]
	

Part 4 - Tests 

Test entrer with "lire la carte / examiner la carte / mettre le cadeau dans la cheminée / poser la hotte / mettre la hotte / enlever la hotte / descendre / frapper à la porte / aller à droite / regarder le gnome / donner le cadeau au gnome / parler avec le gnome / questionner le gnome au sujet de la maison / donner la carte au gnome / prendre la cle / aller à l'ouest / demander les chaussons au gnome / prendre les chaussons / ouvrir la porte avec la clef / ouvrir la porte / entrer"

Test finir with "prendre le vase / éteindre le feu / mettre les chaussons sur les chenets /  sortir / monter /prendre le cadeau / mettre le cadeau dans la cheminée / entrer dans le traineau"

Test me with "test entrer / test finir"
