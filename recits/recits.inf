!% -S
!==============================================================================

Constant Story "R�cits de grand-p�re";
Constant Headline
             "^Fiction interactive �crite par R�mi Verschelde (Akien),^
             pour la quatri�me speed-if francophone.^^";
Release 1;

Constant NO_SCORE;

Replace Identical; ! pour la gestion des adjectifs

Include "Parser";


!--------------------------------------------
! Attributs et propri�t�s
!--------------------------------------------
Property adjective; ! pour la gestion des adjectifs

!--------------------------------------------
! Directives de remplacement
!--------------------------------------------
! (Samuel Verschelde, tir� de pname.h)
! Cette fonction est recopi�e de parserm.h, avec un ajout pour tenir
! compte de la propri�t� "adjective."
[ Identical o1 o2 p1 p2 n1 n2 i j flag;
    if (o1 == o2) rtrue;  ! This should never happen, but to be on the safe side
    if (o1 == 0 || o2 == 0) rfalse;  ! Similarly
    if (parent(o1) == compass || parent(o2) == compass) rfalse; ! Saves time

    !  What complicates things is that o1 or o2 might have a parsing routine,
    !  so the parser can't know from here whether they are or aren't the same.
    !  If they have different parsing routines, we simply assume they're
    !  different.  If they have the same routine (which they probably got from
    !  a class definition) then the decision process is as follows:
    !
    !     the routine is called (with self being o1, not that it matters)
    !       with noun and second being set to o1 and o2, and action being set
    !       to the fake action TheSame.  If it returns -1, they are found
    !       identical; if -2, different; and if >=0, then the usual method
    !       is used instead.

    if (o1.parse_name ~= 0 || o2.parse_name ~= 0) {
        if (o1.parse_name ~= o2.parse_name) rfalse;
        parser_action = ##TheSame; parser_one = o1; parser_two = o2;
        j = wn; i = RunRoutines(o1,parse_name); wn = j;
        if (i == -1) rtrue;
        if (i == -2) rfalse;
    }

    !  This is the default algorithm: do they have the same words in their
    !  "name" (i.e. property no. 1) properties.  (Note that the following allows
    !  for repeated words and words in different orders.)

    p1 = o1.&1; n1 = (o1.#1)/WORDSIZE;
    p2 = o2.&1; n2 = (o2.#1)/WORDSIZE;

    !  for (i=0 : i<n1 : i++) { print (address) p1-->i, " "; } new_line;
    !  for (i=0 : i<n2 : i++) { print (address) p2-->i, " "; } new_line;

    for (i=0 : i<n1 : i++) {
        flag = 0;
        for (j=0 : j<n2 : j++)
            if (p1-->i == p2-->j) flag = 1;
        if (flag == 0) rfalse;
    }

    for (j=0 : j<n2 : j++) {
        flag = 0;
        for (i=0 : i<n1 : i++)
            if (p1-->i == p2-->j) flag = 1;
        if (flag == 0) rfalse;
    }

    ! DEBUT MODIFICATION POUR LES ADJECTIFS
    ! test sur la propri�t� adjective :
    ! deux objets qui ont le m�me "name" mais
    ! pas le m�me "adjective"
    if ((o1 provides adjective) ~= (o2 provides adjective))
        rfalse;

    if (o1 provides adjective && o2 provides adjective)
    {
        n1=(o1.#adjective)/WORDSIZE; n2=(o2.#adjective)/WORDSIZE;

        ! If there are a different number of words in the adjective
        ! properties, they are distinguishable.
        if (n1 ~= n2)
            rfalse;

        p1=o1.&adjective; p2=o2.&adjective;

        for (i=0 : i<n1 : i++)
        {
            flag = 0;
            for (j=0 : j<n2 : j++)
                if (p1-->i == p2-->j) flag = 1;
            if (flag == 0) rfalse;
        }

        for (j=0 : j<n2 : j++)
        {
            flag = 0;
            for (i=0 : i<n1 : i++)
                if (p1-->i == p2-->j) flag = 1;
            if (flag == 0) rfalse;
        }
    }
    ! FIN MODIFICATION POUR LES ADJECTIFS

    !  print "Which are identical!^";
    rtrue;
];

Object  LibraryMessages         ! Pour remplacer certains messages standard
  with  before [;
          Miscellany:
            if (lm_n == 39)
                 "Cela n'a pas la moindre importance.";
          Smell:
            if (location == bunker or bunker_couloir or nomansland) { "L'odeur de sang et de crasse est omnipr�sente en ces lieux."; }
            else { "Aucune odeur singuli�re ne vient alt�rer vos perceptions d�j� troubles."; }
          Listen: 
            if (location == bunker or bunker_couloir or nomansland) { "Vous entendez parfois des personnes parler � l'ext�rieur, mais relativement loin."; }
            else { rfalse; }
          WaveHands:
            "Vous agitez vos petites menottes.";
          CrierSansPrecision:
            "Vous criez ce qui vous passe par la t�te, mais personne ne r�pond.";
          Sing:
            print "Vous chantonnez les premi�res notes du g�n�rique de "; style underline; print "Il faut sauver le soldat Ryan"; style roman; ".";
          Pray:
            "Vous vous agenouillez pour vous recueillir.";
          Think:
            if (location == bunker or bunker_couloir or nomansland) { "Vous pensez... Et vous demandez si vous ne seriez pas en plein cauchemar."; }
            else { "Vous pensez, et vos pens�es vous disent de continuer � jouer."; }
          Sleep:
            if (location == bunker or bunker_couloir or nomansland) { print "Vous vous allongez et vous endormez...^^"; <<Wake>>; }
            else { rfalse; }
          Wake:
            "R�ve ou r�alit�, qui saurait le dire ?";
          Dig:
            "Ce serait absurde.";
          Buy:
            "Rien n'est � vendre en ce lieu.";
          ThrowAt: "� quoi bon ?";
        ];

Include "VerbLib";

! (Samuel Verschelde, tir� et am�lior� de pname.h) gestion des adjectifs
! et gestion aussi des mots de liaison comme dans "boule DE cristal"
! => de est accept�, ainsi que d'autres mots que l'on peut trouver
! entre deux mots d�signant un m�me objet.
[ ParseNoun obj n m p w dict_flags_of_noun;
    dict_flags_of_noun = 0;

    w = NextWord();
    while((WordInProperty(w,obj,name) == 1) || (WordInProperty(w,obj,adjective) == 1) || (w=='de' or 'du' or 'des' or 'd^' or 'la' or 'le' or 'l^' or 'en' or 'a' or 'au' or 'aux'))
    {
        ! remarque : si un mot est � la fois dans name et adjective, il ne sera
        ! compt� qu'une fois, pour la propri�t� name
        if (WordInProperty(w,obj,name) == 1)
        {
            dict_flags_of_noun = ((w->#dict_par1) & $$01110100);
            if (dict_flags_of_noun & 4) ! si c'est un pluriel (//p)
            {
                parser_action = ##PluralFound; ! notifier qu'un pluriel pouvant d�signer
                                               ! plusieurs objets a �t� trouv�
            }
            m++;
            p=0;
        }
        else
        {
            if (WordInProperty(w,obj,adjective) == 1)
            {
                n++;
                p=0;
            }
            else
            {
                ! Si le mot est 'de', 'du', 'des', 'la', 'en', 'a', 'au' ,'aux' :
                if (w=='de' or 'du' or 'des' or 'd^' or 'la' or 'le' or 'l^' or 'en' or 'a' or 'au' or 'aux')
                {
                    ! ajouter 1 � la variable p, qui contient le score � retirer
                    ! au final (afin de revenir en arri�re en cas d'occurrences qui ne
                    ! sont pas comprises entre deux noms reconnus).
                    n++; ! compter ce mot comme pertinent est discutable,
                         ! mais sans cela cela ne fonctionne pas
                    p++;
                }
            }
        }

        w = NextWord();
    }
    wn = wn -p -1; ! retour arri�re car le dernier mot lu n'a pas �t� reconnu
    if (m == 0) return 0; return (m+n-p);
];

!==============================================================================

Class   Decor
  has   scenery concealed;

Attribute chargee;

!==============================================================================

Object  rue "Dans la rue"
  with  name 'rue' 'ruelle' 'route' 'chemin',
        description "Une rue commune, identique � toutes celles de ce quartier, qui se croisent � angle droits. Sur les c�t�s est et ouest se succ�dent de jolies maisons, toutes semblables. La route continue � perte de vue au nord et au sud. Vous vous trouvez devant votre maison, � l'est.",
        initial [;
          KeyCharPrimitive();
          ClearScreen();
          "~Pierre, il va bient�t �tre sept heures, d�p�che-toi de rentrer � la maison !~^^
          Vous d�tournez votre regard de vos billes qui jonchent le sol et regardez votre m�re rentrer chez vous en laissant la porte ouverte, vous invitant � la suivre.";
        ],
        e_vu 0,
        n_to "Allons, vous pouvez bien jouer aux billes quelques minutes de plus. Votre montre n'indique que six heures vingt-trois apr�s tout.",
        s_to "Allons, vous pouvez bien jouer aux billes quelques minutes de plus. Votre montre n'indique que six heures vingt-trois apr�s tout.",
        w_to "Rentrer chez Tommy ? Pourquoi pas.^Vous allez sonner mais personne ne r�pond.",
        e_to [;
          if (self.e_vu == 0) {
            self.e_vu = 1;
            "Votre m�re voudrait vous voir rentrer, mais le soleil est encore haut de le ciel et puis, il n'est que six heures vingt-trois.";
          }
          else {
            print "Finalement c'est vrai qu'il se fait tard, mieux vaut ob�ir � votre m�re. Vous rentrez chez vous et votre m�re vous sert un bon verre de lait.";
            deadflag = 2;
            rtrue;
          }
        ];

Object  heros "vous" rue
  with  name 'heros' 'vous' 'moi' 'protagoniste' 'personnage' 'joueur' 'enfant' 'gosse' 'pierre',
        description "Vous �tes un enfant turbulent, mais pas m�chant, et qui sait tout l'amour que lui portent ses parents. Vous portez une montre au poignet.",
  has   male animate light;

Object  maisons "maisons" rue class Decor
  with  name 'maison' 'maisons' 'foyer',
        adjective 'identique' 'identiques' 'semblable' 'semblables',
        description "De multiples maisons identiques qui s'alignent � l'est et � l'ouest. La votre se trouve derri�re vous, � l'est.",
  has   female pluralname;

Object  montre "montre" heros
  with  name 'montre' 'poignet',
        adjective 'fluo' 'fluorescente',
        article "votre",
        vue 0,
        description [;
          if (self.vue == 0) {
            self.vue = 1;
            "Votre montre fluorescente indique six heures vingt-trois.";
          }
          else { "Votre montre indique toujours six heures vingt-trois. Peut-�tre est-elle cass�e finalement ?"; }
        ],
  has   female worn;

Object  sac "sac de billes" rue
  with  name 'sac' 'sachet',
        adjective 'tissus' 'bille' 'billes',
        article "votre",
        description [;
          print "Un sac de billes en tissus qui contient neuf billes.^";
        ],
        before [;
          Receive:
            if (noun == billes) { rfalse; }
            else { "Ce sac ne peut contenir que des billes."; }
        ],
  has   container open;

Object  billes "billes rouges et bleues" sac
  with  name 'bille' 'billes',
        adjective 'bleue' 'bleu' 'bleues' 'bleus' 'rouge' 'rouges',
        description "Des billes rouges et bleues, de vos couleurs pr�f�r�es.",
  has   female pluralname;

Object  bunker "Dans un bunker d�labr�"
  with  name 'bunker' 'blockhaus' 'casemate' 'fortin',
        adjective 'delabre' 'vetuste',
        description "Ce blockhaus semble manifestement de facture italienne. Les ouvertures sont bien trop hautes pour que vous puissez regarder � l'ext�rieur ou vous hisser, mais vous pouvez observer quelques �l�ments int�ressants, dont une porte en fer � l'ouest.",
        before [;
          Wake:
            if (noun == 0) {
              print "Vous vous r�veillez en sursaut dans votre salon, assis sur le canap� aux c�t�s de votre grand-p�re qui vous racontait ses exploits militaires avant de s'endormir. Le coeur battant, vous allez raconter votre cauchemar � votre m�re qui vous sert un verre de lait en disant qu'il est dangereux d'�couter le v�cu terrifiant de son grand-p�re un peu g�teux lorsque l'on � six ans...";
              deadflag = 2;
              rtrue;
            }
          Pull,VagueFire:
            if (noun == 0) { <<VagueUse breda>>; }
        ],
        w_to porte_beta,
        cant_go "Aucune issue de ce c�t� l�.";

Object  breda "mitrailleuse Breda M37" bunker
  with  name 'mitrailleuse' 'breda' 'm37' 'm1937',
        adjective 'lourde' 'italienne',
        description [;
          print "Une mitrailleuse lourde fix�e dans la paroi et pointant vers l'ext�rieur, sur laquelle vous parvenez � lire : ~Breda M1937~. Votre grand-p�re vous avait parl� de cette fameuse arme qu'utilisaient les italiens durant la guerre de 39-45.";
          if (self has chargee) { " Elle semble charg�e..."; }
          else { " Elle n'est heureusement pas charg�e."; }
        ],
        before [;
          Receive:
            if (noun == chargeur) { rfalse; }
            else { "Vous ne pouvez rien mettre d'autre que des munitions pour Breda M37 dans cette arme."; }
          VagueUse:
            if (self has chargee) {
              move chargeur to thedark;
              give breda ~chargee;
              print "Vous hissant sur la pointe des pieds, vous appuyez sur la d�tente et tirez une salve de munitions dans un bruit assourdissant.^^Vous entendez alors de grands cris et un homme en uniforme s'approche, vous d�visage. Il vous parle dans une langue que vous ne comprenez pas, de l'italien tr�s probablement. Vous l'entendez alors longer le bunker pour descendre � votre niveau, et d�verrouiller la porte qui donne sur l'ext�rieur.^^
              Il arrive le front rouge de rage, �cumant, bouillant comme si de la vapeur lui sortait par les oreilles, et vous agrippe par le col. Vous tra�nant � l'ext�rieur, il vous propulse devant lui sur le "; style underline; print "no man's land"; style roman; print ".^";
              PlayerTo(nomansland);
              rtrue;
            }
            else { "Vous appuyez sur la gachette mais rien ne se produit."; }
          Fire:
            if (noun == self) { <<VagueUse self>>; }
        ],
  has   female static ~chargee;

Object  bunker_couloir "Couloir menant � l'ext�rieur"
  with  name 'bunker' 'blockhaus' 'casemate' 'fortin' 'couloir',
        adjective 'delabre' 'vetuste',
        description "Le couloir chemin d'est en ouest et d�bouche de chaque c�t� sur une porte en fer.",
        before [;
          Wake:
            if (noun == 0) {
              print "Vous vous r�veillez en sursaut dans votre salon, assis sur le canap� aux c�t�s de votre grand-p�re qui vous racontait ses exploits militaires avant de s'endormir. Le coeur battant, vous allez raconter votre cauchemar � votre m�re qui vous sert un verre de lait en disant qu'il est dangereux d'�couter le v�cu terrifiant de son grand-p�re un peu g�teux lorsque l'on � six ans...";
              deadflag = 2;
              rtrue;
            }
        ],
        w_to porte_alpha,
        e_to porte_beta,
        cant_go "Aucune issue de ce c�t� l�.";

Object  porte_beta "porte b�ta"
  with  name 'porte' 'beta',
        adjective 'deuxieme' 'seconde' 'fer' 'lourde' 'blanc',
        description "Une lourde porte de fer sur laquelle est inscrit un grand b�ta en blanc.",
        found_in bunker bunker_couloir,
        door_dir [;
          if (location == bunker) { return w_to; }
          else { return e_to; }
        ],
        door_to [;
          if (location == bunker) { return bunker_couloir; }
          else { return bunker; }
        ],
        before [;
          Attack:
            "Vous frappez � la porte qui �trangement ne r�sonne pas mais produit un son sourd, peu audible.";
        ],
  has   ~open openable door scenery female;

Object  porte_alpha "porte alpha"
  with  name 'porte' 'alpha',
        adjective 'premiere' 'fer' 'lourde' 'blanc',
        description "Une lourde porte de fer sur laquelle est inscrit un grand alpha en blanc.",
        found_in bunker_couloir,
        door_dir w_to,
        door_to "La porte est ferm�e � clef, et vous ne poss�dez pas de quoi l'ouvrir.",
        before [;
          Attack:
            "Vous frappez � la porte qui �trangement ne r�sonne pas mais produit un son sourd, peu audible.";
        ],
  has   ~open locked lockable door scenery female;

Object  malle "malle militaire en fer" bunker_couloir
  with  name 'malle' 'caisse' 'coffre',
        adjective 'militaire' 'fer' 'forge' 'lourde',
        description "Une lourde malle militaire en fer forg� se trouve contre le mur sud.",
  has   ~open openable container female static;

Object  chargeur "chargeur de Breda M37" malle
  with  name 'chargeur' 'chargeurs' 'munitions' 'recharge' 'recharge' 'munition',
        adjective 'breda' 'm37' 'm1937' 'militrailleuse',
        before [;
          Insert:
            if (second == breda) {
              give breda chargee;
              move chargeur to breda;
              "Vous chargez tant bien que mal la mitrailleuse Breda M37, avec le peu de connaissances en ballistiques que vous poss�dez. L'objet vous effraie et vous fascine � la fois.";
            }
          Take:
            if (self in breda) {
              give breda ~chargee;
              move chargeur to player;
              "Vous d�chargez la mitrailleuse Breda M37. C'est en effet plus prudent.";
            }
        ];

Object  nomansland "No man's land"
  with  name 'no' 'man^s' 'land' 'terrain' 'vague',
        description "Vous �tes au milieu de nulle part, sur un terrain vague. Le bunker d�passe du sol, devant vous, et vous pouvez sentir le regard du soldat italien peser sur vous depuis derri�re la mitrailleuse, de m�me que les regards de nombreux autres militaires dans les tranch�es. Au fur et � mesure que passe le temps, le bunker se fait plus distant...",
        scenic_look 'cordellino' 'serge' 'gris' 'clair' 0 "L'uniforme classique des officiers italiens durant la Seconde Guerre mondiale, tel que vous l'a d�crit votre grand-p�re."
        'bottes' 'hautes' 'cuir' 'noir' 0 "Des bottes semblables � celles des militaires allemands."
        'bunker' 'blockhaus' 'casemate' 'fortin' 0 "Le bunker vous semble s'�loigner...",
        scenic_smell 'soldats' 'militaires' 'officiers' 'fascistes' 'italiens' 0 "Vous �tes loin d'eux, et pourtant vous parvenez � sentir l'odeur de crasse qui les impr�gne.",
        scenic_listen 'soldats' 'militaires' 'officiers' 'fascistes' 'italiens' 0 "Vous les entendez chuchoter en vous observant.",
        scenic_allactions 'soldats' 'militaires' 'officiers' 'fascistes' 'italiens' 0 "De nombreux militaires vous observent depuis les tranch�es, mais restent muets � vos supplications.",
        before [;
          Wake:
            if (noun == 0) {
              print "Vous vous r�veillez en sursaut dans votre salon, assis sur le canap� aux c�t�s de votre grand-p�re qui vous racontait ses exploits militaires avant de s'endormir. Le coeur battant, vous allez raconter votre cauchemar � votre m�re qui vous sert un verre de lait en disant qu'il est dangereux d'�couter le v�cu terrifiant de son grand-p�re un peu g�teux lorsque l'on � six ans...";
              deadflag = 2;
              rtrue;
            }
          Dig:
            move billes to sac;
            move sac to player;
            "Vous creusez et trouvez... un sac de billes !";
        ],
        cant_go [;
          print "Vous esquissez un mouvement mais l'officier italien tire une salve de balles pour vous couper la route.";
        ];

Object  soldat "soldat italien" nomansland class Decor
  with  name 'soldat' 'italien' 'militaire' 'fasciste' 'officier',
        description [;
          print "Vous pouvez plus le d�tailler d�sormais : c'est visiblement un officier italien v�tu d'un "; style underline; print "cordellino"; style roman; " en serg� gris clair et portant de hautes bottes de cuir noir.";
        ],
        before [;
          Attack:
            print "Vous tentez de l'approcher mais l'officier tire une salve de balles de Brenda M37...^^
            Vous vous r�veillez en sursaut dans votre salon, assis sur le canap� aux c�t�s de votre grand-p�re qui vous racontait ses exploits militaires avant de s'endormir. Le coeur battant, vous allez raconter votre cauchemar � votre m�re qui vous sert un verre de lait en disant qu'il est dangereux d'�couter le v�cu terrifiant de son grand-p�re un peu g�teux lorsque l'on � six ans...";
            deadflag = 2;
            rtrue;
        ],
        life [;
          Look,Examine:
          Tell,Ask,AskFor,Answer,ParlerSansPrecision: "Vous balbutiez quelques mots en fran�ais et l'officier vous r�pond avec un fort accent :^^
          ~Pas oune geste sinon je te toue, comunista !~^^
          Ah, si seulement vous aviez �cout� votre m�re ! Si seulement vous pouviez vous r�veiller, sortir de ce cauchemar affreux.";
          default: "Il est trop loin de vous et il vous fait signe de ne pas bouger.";
        ],
  has   male animate;

!==============================================================================

[ Initialise;
  lookmode = 2;
  ChangePlayer(heros);
  print "Si vous jouez pour la premi�re fois � une fiction interactive, tapez "; style underline; print "aide"; style roman; print " une fois l'introduction pass�e.^^";
];

[ HelpSub;
  print "Ce programme est un jeu d'aventure textuel, ou "; style underline; print "fiction interactive"; style roman; print " et se base donc sur un dialogue entre le joueur, vous, et sa machine. Ainsi vous pouvez interagir avec le milieu environnant � travers des ordres � l'infinitif, tels que ceux-ci :^"; print " > regarder les murs interieurs^ > prendre les gelules^ > ouvrir la fenetre^^Il peut parfois arriver (voire assez souvent pour un joueur n�ophyte) que vos instructions ne soient pas comprises, vous pouvez alors r�essayer en reformulant d'une mani�re plus claire et simple (l'association "; style underline; print "verbe"; style roman; print " + "; style underline; print "compl�ment"; style roman; print " est la plus courante).^^"; print "Pour d�placer votre personnage de lieux en lieux, utilisez les huits points cardinaux ("; style underline; print "nord, nord-est, est, sud-est, sud, sud-ouest, ouest, nord-ouest, nord"; style roman; print ") ainsi qu'"; style underline; print "entrer, sortir, haut, bas"; style roman; print ".^Pour avoir de nouveau la description d'un lieu qui appara�t lorsque l'on s'y rend, tapez "; style underline; print "regarder"; style roman; print " lorsque vous vous y trouvez, et pour lister vos acquisitions, tapez "; style underline; print "inventaire"; style roman; print ".^^Vous pouvez aussi utiliser les commandes "; style underline; print "annuler, sauvegarder, charger, recommencer"; style roman; print " et "; style underline; print "quitter"; style roman; print ".^^Enfin le site "; style underline; print "http://ifiction.free.fr"; style roman; print " et toute la communaut� fran�aise de fiction interactive sont � votre disposition.^";
];

[ PlaySub;
  if ((billes in location) || (billes in sac) || (billes in player)) {
    print "Vous tapez d'une pichenette dans une bille qui vient en heurter une seconde.^^
    Votre m�re sort � nouveau de la maison : ~Pierre, il est tard ! Rentre tout de suite o� le bonhomme sept-heures viendra t'enlever !~^^
    D�daignant son injonction, vous continuez � jouer quand soudain...^^";
    move billes to rue;
    KeyCharPrimitive();
    print "Un vieil homme � l'air s�rieux, portant un chapeau melon, une longue cape noire et un sac sur son dos, s'approche claudiquant � l'aide d'une canne de bois clair. Arriv� � votre hauteur, il prend une poign�e de sable dans son sac et vous la lance au visage. Vous �tes aveugl� mais comprenez cependant qu'il vous emporte sur son �paule, tandis qu'au loin r�sonne la voix de votre m�re :^^
    ~Je t'avais pr�venu que le bonhomme sept-heures aller passer ! Si tu avais �cout� ta m�re, �a ne serait pas arriv� ! Ah, j'aurais d� rendre son paquet � la cigogne qui t'a confi� � moi.~^^";
    KeyCharPrimitive();
    ClearScreen();
    print "Sans trop savoir comment, vous reprenez connaissance seul dans un blockhaus datant, � ce que laissent penser les faisceaux romains grav�s sur les murs au couteau, du fascisme italien. Un peu comme dans les "; style underline; print "films"; style roman; print " qu'avait tourn�s votre grand-p�re durant la Seconde Guerre mondiale � l'aide du kin�tographe de son propre a�eul. Vous vous souvenez des temps amusants o� vous vous �merveilliez devant son kin�toscope centenaire, malgr� l'atrocit� des images qu'il vous montrait.^";
    PlayerTo(bunker);
  }
  else { "Jouer avec quoi ?"; }
];

[ PlayBillesSub;
  if (noun == billes) { <<Play>>; }
  else { "Vous ne voulez pas jouer avec �a."; }
];

[ FireSub;
  "Il n'y a aucune arme utilisable ici.";
];

[ VagueFireSub;
  "Pouvez-vous �tre plus pr�cis ?";
];

[ LoadSub;
  if (noun == breda && chargeur in player) {
    give breda chargee;
    move chargeur to breda;
    "Vous chargez tant bien que mal la mitrailleuse Breda M37, avec le peu de connaissances en ballistiques que vous poss�dez. L'objet vous effraie et vous fascine � la fois.";
  }
  else if (noun == breda) { "Vous n'avez pas de quoi charger la mitrailleuse Breda M37."; }
  else { print_ret "Vous ne pouvez pas charger ", (the) noun, "."; }
];

!==============================================================================

Global ScenicFlag = 4;

Constant ScenicErrorOther = "Vous n'y voyez aucune utilit�.";
Constant ScenicErrorLook = NULL; ! utiliser le message par d�faut Miscellany 39
Constant ScenicErrorSmell = NULL; ! utiliser le message par d�faut pour "Smell"
Constant ScenicErrorListen = NULL; ! utiliser le message par d�faut pour "Listen"
Constant ScenicErrorTouch = "Vous n'y voyez aucune utilit�.";
Constant ScenicErrorTaste = "C'est non comestible, selon toute �vidence."; ! utiliser le message par d�faut pour "Taste"
Include "scenic5sens";

! Version 1.0

Include "FrenchG";

! Version 2.3 des biblioth�ques francophones

!==============================================================================

Verb "help" "aide" "manuel" "manual" "info" "information" "menu"
    *                                           -> Help;

Verb "jouer"
  *                                             -> Play
  * 'aux'/'avec' noun                           -> PlayBilles;

Extend "tirer" first
  * 'avec' noun                                 -> Fire;

Extend "faire" first
  * 'feu'                                       -> VagueFire
  * 'feu' 'avec' noun                           -> Fire;

Extend "charger"
  * noun                                        -> Load;
